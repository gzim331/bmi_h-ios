//
//  ViewController.swift
//  bmi_h
//
//  Created by michal on 27/07/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    private var bmi: Bmi!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        weightTextField.resignFirstResponder()
    }
    
    func errorMessage(content: String) {
        resultLabel.textColor = UIColor.red
        resultLabel.text = content
    }
    
    func successMessage(content: String) {
        resultLabel.textColor = UIColor.black
        resultLabel.text = content
    }
    
    @IBAction func weightPressed(_ sender: Any) {
        if let weight = Float(weightTextField.text!) {
            if weight > 0 {
            bmi = Bmi(weight: weight)
            let height = bmi.countingGrowth()
            
            successMessage(content: height)
            weightTextField.resignFirstResponder()
                
            } else {
                errorMessage(content: "Waga musi być dodatnia")
            }
        } else {
            errorMessage(content: "Podaj wagę")
        }
        weightTextField.resignFirstResponder()
    }
    
}
