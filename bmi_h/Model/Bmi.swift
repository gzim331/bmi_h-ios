//
//  Bmi.swift
//  bmi_h
//
//  Created by michal on 31/07/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import Foundation

class Bmi {
    
    let weight: Float?
    
    var height_19: Int = 0
    var height_24: Int = 0
    
    init(weight: Float) {
        self.weight = weight
    }
    
    func countingGrowth() -> String {
        height_19 = Int(sqrt(weight!/19.0)*100)
        height_24 = Int(sqrt(weight!/24.0)*100)
        
        return "Zakres wzrostu: \(height_24)cm - \(height_19)cm"
    }
}
